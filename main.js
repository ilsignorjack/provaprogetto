fetch('./reviews.json').then((response) => response.json()).then((data) =>{

    let revWrapper = document.querySelector('#revWrapper')

    function showReviews(array){

        revWrapper.innerHTML = '';


        array.forEach((el)=>{
            let div = document.createElement('div')
            div.classList.add('col-12', 'col-md-3')
            div.innerHTML = `
            <img src="${el.img}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title text-center">${el.titolo}</h5>
            </div>
            `
            revWrapper.appendChild(div)

        })
    }

    showReviews(data)

})

fetch('./evidenzia.json').then((response) => response.json()).then((data) =>{

    let evidenziaWrapper = document.querySelector('#evidenziaWrapper')

    function showEvidenzia(array){

        evidenziaWrapper.innerHTML = '';


        array.forEach((el)=>{
            let div = document.createElement('div')
            div.classList.add('col-12', 'col-md-3')
            div.innerHTML = `
            <img src="${el.img}" class="card-img-top" alt="...">
            <div class="card-body">
              <h3 class="card-title text-center">${el.descrizione}</h3>
            </div>
            `
            evidenziaWrapper.appendChild(div)

        })
    }

    showEvidenzia(data)

})